function formatPhone (input) {
  var phone = input.replace(/\D/g,'');
  if ( phone.length >= 11) {
    phone = phone.substring(1,11);
  }
  var areaCode = phone.substring(0,3);
  var prefix = phone.substring(6,3);
  var number = phone.substring(10,6);

  return areaCode + " " + prefix + " " + number;
}