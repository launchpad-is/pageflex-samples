using Pageflex.Scripting;
using System;
using System.Net;

namespace PageFlexActions
{
    public class Symbology
    {
        public static void CreateQRCode(string data, string width, string height)
        {
	    if (!string.IsNullOrWhiteSpace(data))
            foreach (Image i in Application.CurrentDocument.Images)
            {
                if (i.Name == "QRCode")
                {
                    string newFileName = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString();
                    using (WebClient wc = new WebClient())
                    {
                        wc.DownloadFile(string.Format(
                            "https://api.qrserver.com/v1/create-qr-code/?data={0}&amp;size={1}x{2}",
                            Uri.EscapeDataString(data),
                            width,
                            height), newFileName);

                        i.Source = newFileName;
                        Application.EvaluateVariables();
                        Application.MergeData();
                    }
                }
            }
        }
    }
}
