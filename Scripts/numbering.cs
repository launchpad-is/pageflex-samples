using Pageflex.Scripting;
using System;
using System.Net;

namespace PageFlexActions
{
    public class Numbering
    {
        public static void NumberAndRepeat(string VariableName, string StartingNumber, string Quantity, string NumberLength = "6")
        {
            // Make sure we're not missing a value we need.
            if (!string.IsNullOrWhiteSpace(VariableName) && !string.IsNullOrWhiteSpace(StartingNumber) && !string.IsNullOrWhiteSpace(Quantity))
            {
                int startNumber = Convert.ToInt32(StartingNumber);
                int quantity = Convert.ToInt32(Quantity);
                int numberLength = Convert.ToInt32(NumberLength);

                for (uint i = 1; i <= quantity; i++)
                {
                    startNumber += 1;
                    Page page = Application.CurrentDocument.GetPage(i);                   
                    Application.GetVariable(VariableName).Value = startNumber.ToString().PadLeft(numberLength, '0');
                    Application.EvaluateVariables();
                    Application.MergeData();

                    page.Duplicate();
                    page.RemoveVariables(false);
                }

                
            }
        }
    }
}